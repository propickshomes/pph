import React from 'react'
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import carousel1 from '../Images/carousel1.png'
import carousel2 from '../Images/carousel2.png'
import carousel3 from '../Images/carousel3.png'

function HomePage() {
    const carouselStyles = {
        height: '400px',
        margin: ' 0px'
      }
  return (
    <div>
         <Carousel autoPlay interval="5000" transitionTime="5000" showThumbs={false} infiniteLoop={true}>
          <div style={carouselStyles}>
            <img src={carousel1} alt="" style={{ height: '400px' }} />
          </div>
          <div style={carouselStyles}>
            <img src={carousel2} alt='' style={{ height: '400px' }} />
          </div>
          <div style={carouselStyles}>
            <img src={carousel3} alt='' style={{ height: '400px' }} />
          </div>
        </Carousel>
    </div>
  )
}

export default HomePage