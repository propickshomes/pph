import React from 'react'
import Image from '../Images/logo.png'

function NavigationBar() {
  return (
    <div>
      <nav class="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: '#e3f2fd' }}>
        <img src={Image} alt='' height={60} width={200}></img>
        <h1>ProPicks Homes</h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">

          <ul class='navbar-nav mx-auto' style-ml>
            <li class="nav-link active p-2" href="#"><h4>HomePage</h4></li>
            <li class="nav-link active p-2" href="#"><h4>AboutUs</h4></li>
            <li class="nav-link active p-2" href="#"><h4>ContactUs</h4></li>
            <li class="nav-link active p-2" href="#"><h4>Register</h4></li>
            <li class="nav-link active p-2" href="#"><h4>Login</h4></li>
          </ul>
        </div>

      </nav>

    </div>
  )
}

export default NavigationBar