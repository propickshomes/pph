import React from 'react'
import { MDBFooter, MDBContainer,MDBRow,MDBCol} from 'mdb-react-ui-kit';

function Footer() {
  return (
    <div>

    <MDBFooter className='bg-secondary text-white text-center'>
     <MDBContainer className='p-4'>
       <MDBRow>
         <MDBCol lg="12" md="12" className='mb-4 mb-md-0'>
           <h5>ProPicks Homes</h5>

           <p>
           ProPicks Homes is your premier source for buying, selling, and rental homes. Our dedicated team of experts is committed to providing you with top-notch service and guidance to make your real estate journey seamless and successful.
           Learn more about ProPicks Homes and our commitment to excellence in real estate. Visit our About Us page to meet our team and discover what makes us your trusted partner in real estate.
           </p>
         </MDBCol>
       </MDBRow>
     </MDBContainer>

     <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
     Copyright © 2024 ProPicks Homes. All rights reserved.
     </div>
   </MDBFooter>
</div>
  )
}

export default Footer